heeeeCXX=g++
CXXFLAGS=-Wall -pedantic --std=c++11 -Wno-long-long -O0 -ggdb
LD=g++
LDFLAGS=-Wall -pedantic --std=c++11 -Wno-long-long -O0 -ggdb

# komentar
# $< promenna pro prvni predpoklad
# $^ promenna pro vsechny predpoklady
# $@ promenna pro jmeno cile 


all: compile doc

clean:
	rm ./src/*.o krychmir	
	rm -r ./doc	

doc:
	mkdir doc	
	cd src; doxygen config	
	
compile: ./src/main.o ./src/manager.o ./src/game.o ./src/object.o ./src/point.o ./src/monster.o ./src/wall.o ./src/turret.o
	$(LD) $(LDFLAGS) $^ -o krychmir	

	
run:	
	./krychmir	


main.o: ./src/main.cpp ./src/manager.h	
	$(LD) $(LDFLAGS) -c ./src/main.cpp -o ./src/	
manager.o: ./src/manager.cpp ./src/manager.h ./src/game.h
	$(LD) $(LDFLAGS) -c ./src/manager.cpp -o ./src/	
game.o: ./src/game.cpp ./src/game.h ./src/object.h ./src/monster.h ./src/turret.h ./src/wall.h ./src/point.h
	$(LD) $(LDFLAGS) -c ./src/game.cpp -o ./src/	
object.o: ./src/object.cpp ./src/object.h
	$(LD) $(LDFLAGS) -c ./src/object.cpp -o ./src/	
point.o: ./src/point.cpp ./src/point.h	
	$(LD) $(LDFLAGS) -c ./src/point.cpp -o ./src/	
monster.o: ./src/monster.cpp ./src/monster.h ./src/object.h
	$(LD) $(LDFLAGS) -c ./src/monster.cpp -o ./src/	
wall.o: ./src/wall.cpp ./src/wall.h ./src/object.h
	$(LD) $(LDFLAGS) -c ./src/wall.cpp -o ./src/
turret.o: ./src/turret.cpp ./src/turret.h ./src/object.h ./src/monster.h	
	$(LD) $(LDFLAGS) -c ./src/turret.cpp -o ./src/	


