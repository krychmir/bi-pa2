#include "game.h"

CGame::CGame(){ 

	m_nextId = 1;
	m_win = false;
	m_money = 99999999;
	m_lives = 50000;
	m_dim = CPoint(15,10);
	m_start = CPoint(2,3);
	m_end = CPoint(14,8);
	m_turn = 1;
	
	
	m_path.push_back(CPoint(2,3));
	m_path.push_back(CPoint(3,3));
	m_path.push_back(CPoint(3,4));
	m_path.push_back(CPoint(3,5));
	m_path.push_back(CPoint(2,5));
	m_path.push_back(CPoint(2,6));
	m_path.push_back(CPoint(2,7));
	m_path.push_back(CPoint(2,8));
	m_path.push_back(CPoint(3,9));
	m_path.push_back(CPoint(4,8));
	m_path.push_back(CPoint(5,8));
	m_path.push_back(CPoint(6,9));
	m_path.push_back(CPoint(7,8));
	m_path.push_back(CPoint(7,7));
	m_path.push_back(CPoint(7,6));
	m_path.push_back(CPoint(6,5));
	m_path.push_back(CPoint(6,4));
	m_path.push_back(CPoint(7,3));
	m_path.push_back(CPoint(8,3));
	m_path.push_back(CPoint(9,3));
	m_path.push_back(CPoint(10,3));
	m_path.push_back(CPoint(11,2));
	m_path.push_back(CPoint(12,2));
	m_path.push_back(CPoint(13,2));
	m_path.push_back(CPoint(14,3));
	m_path.push_back(CPoint(14,4));
	m_path.push_back(CPoint(13,5));
	m_path.push_back(CPoint(13,6));
	m_path.push_back(CPoint(12,6));
	m_path.push_back(CPoint(11,6));
	m_path.push_back(CPoint(10,7));
	m_path.push_back(CPoint(10,8));
	m_path.push_back(CPoint(11,9));
	m_path.push_back(CPoint(12,8));
	m_path.push_back(CPoint(13,8));
	m_path.push_back(CPoint(14,8));
	
	spawnWall(CPoint(1,1), new CWall(m_nextId));
	spawnWall(CPoint(2,1), new CWall(m_nextId));
	spawnWall(CPoint(3,1), new CWall(m_nextId));
	spawnWall(CPoint(4,1),new CWall(m_nextId));
	spawnWall(CPoint(5,1),new CWall(m_nextId));
	spawnWall(CPoint(6,1),new CWall(m_nextId));
	spawnWall(CPoint(7,1),new CWall(m_nextId));
	spawnWall(CPoint(8,1),new CWall(m_nextId));
	spawnWall(CPoint(9,1),new CWall(m_nextId));
	spawnWall(CPoint(10,1),new CWall(m_nextId));
	spawnWall(CPoint(11,1),new CWall(m_nextId));
	spawnWall(CPoint(12,1),new CWall(m_nextId));
	spawnWall(CPoint(13,1),new CWall(m_nextId));
	spawnWall(CPoint(14,1),new CWall(m_nextId));
	spawnWall(CPoint(15,1),new CWall(m_nextId));
	
	spawnWall(CPoint(1,2),new CWall(m_nextId));
	spawnWall(CPoint(15,2),new CWall(m_nextId));
	spawnWall(CPoint(1,3),new CWall(m_nextId));
	spawnWall(CPoint(15,3),new CWall(m_nextId));
	spawnWall(CPoint(1,4),new CWall(m_nextId));
	spawnWall(CPoint(15,4),new CWall(m_nextId));
	spawnWall(CPoint(1,5),new CWall(m_nextId));
	spawnWall(CPoint(15,5),new CWall(m_nextId));
	spawnWall(CPoint(1,6),new CWall(m_nextId));
	spawnWall(CPoint(15,6),new CWall(m_nextId));
	spawnWall(CPoint(1,7),new CWall(m_nextId));
	spawnWall(CPoint(15,7),new CWall(m_nextId));
	spawnWall(CPoint(1,8),new CWall(m_nextId));
	spawnWall(CPoint(15,8),new CWall(m_nextId));
	spawnWall(CPoint(1,9),new CWall(m_nextId));
	spawnWall(CPoint(15,9),new CWall(m_nextId));
	
	spawnWall(CPoint(1,10),new CWall(m_nextId));
	spawnWall(CPoint(2,10),new CWall(m_nextId));
	spawnWall(CPoint(3,10),new CWall(m_nextId));
	spawnWall(CPoint(4,10),new CWall(m_nextId));
	spawnWall(CPoint(5,10),new CWall(m_nextId));
	spawnWall(CPoint(6,10),new CWall(m_nextId));
	spawnWall(CPoint(7,10),new CWall(m_nextId));
	spawnWall(CPoint(8,10),new CWall(m_nextId));
	spawnWall(CPoint(9,10),new CWall(m_nextId));
	spawnWall(CPoint(10,10),new CWall(m_nextId));
	spawnWall(CPoint(11,10),new CWall(m_nextId));
	spawnWall(CPoint(12,10),new CWall(m_nextId));
	spawnWall(CPoint(13,10),new CWall(m_nextId));
	spawnWall(CPoint(14,10),new CWall(m_nextId));
	spawnWall(CPoint(15,10),new CWall(m_nextId));
	
	spawnWall(CPoint(5,2),new CWall(m_nextId));
	spawnWall(CPoint(5,3),new CWall(m_nextId));
	spawnWall(CPoint(5,4),new CWall(m_nextId));
	spawnWall(CPoint(5,5),new CWall(m_nextId));
	spawnWall(CPoint(5,6),new CWall(m_nextId));
	spawnWall(CPoint(4,6),new CWall(m_nextId));
	spawnWall(CPoint(3,6),new CWall(m_nextId));
	
	spawnWall(CPoint(8,9),new CWall(m_nextId));
	spawnWall(CPoint(9,9),new CWall(m_nextId));
	spawnWall(CPoint(8,8),new CWall(m_nextId));
	spawnWall(CPoint(9,8),new CWall(m_nextId));
	spawnWall(CPoint(8,7),new CWall(m_nextId));
	spawnWall(CPoint(9,7),new CWall(m_nextId));
	spawnWall(CPoint(8,6),new CWall(m_nextId));
	spawnWall(CPoint(9,6),new CWall(m_nextId));
	spawnWall(CPoint(8,5),new CWall(m_nextId));
	spawnWall(CPoint(9,5),new CWall(m_nextId));
	spawnWall(CPoint(8,4),new CWall(m_nextId));
	spawnWall(CPoint(9,4),new CWall(m_nextId));
	
	spawnWall(CPoint(10,4),new CWall(m_nextId));
	spawnWall(CPoint(11,4),new CWall(m_nextId));
	spawnWall(CPoint(12,4),new CWall(m_nextId));
	spawnWall(CPoint(13,4),new CWall(m_nextId));
	
	spawnWall(CPoint(12,7),new CWall(m_nextId));
	spawnWall(CPoint(13,7),new CWall(m_nextId));
	spawnWall(CPoint(14,7),new CWall(m_nextId));
	
	spawnWall(CPoint(12,9),new CWall(m_nextId));
	spawnWall(CPoint(13,9),new CWall(m_nextId));
	spawnWall(CPoint(14,9),new CWall(m_nextId));
	
	queMonster(new CRunner(m_nextId, 99, 99));
	queMonster(new CTank(m_nextId, 999, 999, 1));
	queMonster(new CRunner(m_nextId, 90, 100));
	queMonster(new CBoss(m_nextId, 909, 109));
	
	addSpawn(1,1);
	addSpawn(2,1);
	addSpawn(3,1);
	addSpawn(4,1);
	addSpawn(5,1);
	addSpawn(6,1);
	addSpawn(7,1);
	
	spawnMonster(CPoint(2,6), new CMonster(m_nextId, 10, 10));
	spawnMonster(CPoint(14,4), new CBoss(m_nextId, 10, 10));
	
	spawnTurret(CPoint(3,6),new CTurret(m_nextId, 10));
	spawnTurret(CPoint(10,4),new CTurret(m_nextId, 10));
	spawnTurret(CPoint(11,4),new CTurret(m_nextId, 10));
	
	spawnTurret(CPoint(5,3),new CFrost(m_nextId, 10));
	
	spawnTurret(CPoint(8,5),new CSniper(m_nextId, 10));
	spawnTurret(CPoint(8,6),new CSniper(m_nextId, 10));
	
	spawnTurret(CPoint(12,7),new CBFG(m_nextId, 10));
	spawnTurret(CPoint(12,9),new CBFG(m_nextId, 10));
	
}
//-----------------------------------------------------------------------------------

CGame::CGame(const std::string & url){

	m_nextId = 1;
	m_win = false;
	
	if(!load(url)) throw std::string("Invalid file");

}

//-----------------------------------------------------------------------------------

CGame::~CGame(){

	for(auto it = m_map.begin(); it != m_map.end(); it++){
	
		delete it->second;
	
	}
	while(!m_monstersToSpawn.empty()){
	
		delete m_monstersToSpawn.front();
		m_monstersToSpawn.pop();
	
	}
	
}

//-----------------------------------------------------------------------------------

void CGame::spawnWall(const CPoint & point, CWall * wall){

	m_walls.insert(std::pair<CPoint, CWall*> (point, wall));
	m_map.insert(std::pair<CPoint, CObject*> (point, wall));
	
	m_nextId++;

}

//-----------------------------------------------------------------------------------

void CGame::spawnMonster(const CPoint & point, CMonster * mons){

	m_monsters.insert(std::pair<CPoint, CMonster*> (point, mons));
	m_map.insert(std::pair<CPoint, CObject*> (point, mons));
	
	m_nextId++;

}

//-----------------------------------------------------------------------------------

void CGame::spawnTurret(const CPoint & point, CTurret * turr){

	m_turrets.insert(std::pair<CPoint, CTurret*> (point, turr));
	m_map.insert(std::pair<CPoint, CObject*> (point, turr));
	
	m_nextId++;

}

//-----------------------------------------------------------------------------------

void CGame::print(std::iostream & os) const{

  os << "Turn: " << m_turn << '\n';
  os << "Lives: " << m_lives << '\n';
  os << "Money: " << m_money << '\n' << '\n';
  
  os << std::setw(2) << " ";

	for(int j = 1; j <= m_dim.getX(); j ++) os << std::setw(3) << j ;
	os << '\n' << '\n';
	for(int i = 1; i <= m_dim.getY(); i ++){
		os << std::setw(2) << i ;
		for(int j = 1; j <= m_dim.getX(); j ++){
			auto iter = m_map.equal_range(CPoint(j,i));	
			if(iter.first != iter.second){ 
				os << std::setw(3);
				iter.second--;
				os << *(iter.second->second);
			}
			else os << std::setw(3) << " " ;
		}
		os << '\n';
	}
}

//-----------------------------------------------------------------------------------

void CGame::nextTurn(){

	moveMonsters();
	
	spawnMonsters();
	
	fireTurrets();

	endTurn();
	

}

//-----------------------------------------------------------------------------------

void CGame::endTurn(){
	
	//Refresne všem monstrům jejich tahy a zruší mrtvá monstra
	for(auto it = m_monsters.begin(); it != m_monsters.end(); it++){
	
		it->second->turn();
		
		if(!(it->second->isAlive()))
		{
			m_money += it->second->moneyInPocket();
			deleteMonster(it, true);
			it = m_monsters.begin();
			if(it == m_monsters.end()) break;
		}
	
	}
	
	
	//Podívá se jestli se monstra dostala do konce
	auto iter = m_monsters.equal_range(m_end);
	
	while( iter.first != iter.second){
		
		m_lives -= iter.first->second->getAttack();
	
		deleteMonster(iter.first, true); //Smaže monstrum z m_monsters a m_map a uvolní paměť
		iter = m_monsters.equal_range(m_end); //Protože jsme smazali musíme vytvořit nový iterátor
	
	}
	
	if(m_monstersToSpawn.empty() && m_monsters.empty()) m_win = true;
	
	m_turn ++;

}

//-----------------------------------------------------------------------------------

void CGame::moveMonsters(){

	auto curr = m_path.begin();
	
	while(*curr != m_end){
	
		auto iter = m_monsters.equal_range(*curr);
		
		while( iter.first != iter.second){		
			
			int range = iter.first->second->moves();
			
			if(iter.first->second->getMoved() < range){ //Zjistí zda monstrum může jít dál			
			
				moveMonster(*(curr+1), iter.first->second); //Pusune monstrum o jeden dále
				deleteMonster(iter.first); //Smaže monstrum z m_monsters a m_map	
				iter = m_monsters.equal_range(*curr); //Protože jsme smazali musíme vytvořit nový iterátor
			}
			else iter.first++;
		
		}	
		
		curr++;
	}

}
//-----------------------------------------------------------------------------------

void CGame::moveMonster(const CPoint & point, CMonster * mons){

	if(mons->getMoved() >= mons->moves()) return;
	mons->setMoved(mons->getMoved()+1);
	
	spawnMonster(point, mons);

}

//-----------------------------------------------------------------------------------

bool CGame::deleteMonster(std::multimap<CPoint, CMonster*>::iterator & mons, bool fullDell){

	int id = mons->second->getId();
	CPoint point = mons->first;
	
	m_monsters.erase(mons);	

	auto iter = m_map.equal_range(point);
	
	while(iter.first != iter.second){
	
		if(iter.first->second->getId() == id){
				if(fullDell) delete iter.first->second; //Monstrum bylo zabyto nebo doslo do cíle / Monster was kiled or it made it to end
				m_map.erase(iter.first);
				return true;
			}
	
		iter.first++;
	}
	
	return false;

}

//-----------------------------------------------------------------------------------

int CGame::getLives(){

	return m_lives;

}

//-----------------------------------------------------------------------------------

bool CGame::getWin(){

	return m_win;

}

//-----------------------------------------------------------------------------------

void CGame::spawnMonsters(){

	auto iter = m_spawns.find(m_turn);
	if(iter == m_spawns.end()) return;
	//Najde čislo kola v m_spawn a pokud tam je tak oživí tolik monster z que kolik bylo ve m_spawn
	//Finds curr turn in m_spawn. On succes spawns x-amount of monster form que
	for(int i = iter->second; i > 0 && !m_monstersToSpawn.empty(); i--){
	
		spawnMonster(m_start, m_monstersToSpawn.front());
		m_monstersToSpawn.pop();
	
	}

}
//-----------------------------------------------------------------------------------

bool CGame::lvlUpTurret(int x, int y){

	auto iter = m_turrets.find(CPoint(x,y));
	if(iter == m_turrets.end()) return false; //Věž existuje / turret exists
	
	if(iter->second->costLvlUp() <= m_money){ //Cena je menší nebo rovna / cost is lower or equeal to money
		m_money -= iter->second->costLvlUp();
		iter->second->lvlUp();
		return true;
	}
	else return false; 

}

//-----------------------------------------------------------------------------------

void CGame::fireTurrets(){ //Projde všechny věže | Pro každou znich najde cíl tím že projde cestu od konce / Goes thrue all turrets | For each one (if posible) finds a target

	for(auto turr = m_turrets.begin(); turr != m_turrets.end(); turr++){ // projde všechny věže / Goes thrue all turrets
	
		for(auto it = m_path.begin(); it != m_path.end(); it++){ // projede celou cestu / Goes thrue path
		
			auto mons = m_monsters.equal_range(*it);
		
			if(*it - turr->first <= turr->second->range() && mons.first != mons.second){ // Pole je na dostřel a je tam monstrum/a / Point is in range and monster is there
				
				for(;mons.first != mons.second; mons.first++){ // projde všechna monstra v poli / Goes thrue all monsters
				
					if(mons.first->second->isAlive()) //pokud je monstrum naživu tak na nej věř vystřelí / If monster is alive, shot it	
					{
						turr->second->attack(*(mons.first->second)); 
						break; // Každá věž střílí pouze jednou / each turret fires just once
					} 
				}	 
			}	
		}
	}
}

//-----------------------------------------------------------------------------------

void CGame::queMonster(CMonster * mons){

	m_monstersToSpawn.push(mons);

}

//-----------------------------------------------------------------------------------

void CGame::addSpawn(int turn, int amount){

	m_spawns.insert(std::pair<int, int> (turn, amount));

}

//-----------------------------------------------------------------------------------

bool CGame::buidTurret(char type, int x, int y){

	// Builds a turret on x y and removes money from player
			
	if(m_walls.find(CPoint(x,y)) == m_walls.end()) return false;
	if(m_turrets.find(CPoint(x,y)) != m_turrets.end()) return false;

	switch(type){ //Types of turrets
	
		case 'B': {
		
			if(CTurret(0,1).costToBuild() <= m_money){ //Checks if player has enought money
			
				spawnTurret(CPoint(x,y),new CTurret(m_nextId, 1));
				m_money -= CTurret(0,1).costToBuild();
				return true;
				
			}
		
			break;
		}
		case 'F': {
		
			if(CFrost(0,1).costToBuild() <= m_money){
			
				spawnTurret(CPoint(x,y),new CFrost(m_nextId, 1));
				m_money -= CFrost(0,1).costToBuild();
				return true;
				
			}
		
			break;
		}
		case 'S': {
			
			if(CSniper(0,1).costToBuild() <= m_money){
			
				spawnTurret(CPoint(x,y),new CSniper(m_nextId, 1));
				m_money -= CSniper(0,1).costToBuild();
				return true;
				
			}
		
			break;
		}
		case 'G': {
			
			if(CBFG(0,1).costToBuild() <= m_money){
			
				spawnTurret(CPoint(x,y),new CBFG(m_nextId, 1));
				m_money -= CBFG(0,1).costToBuild();
				return true;
				
			}
		
			break;
		}
	
	}
	
	return false;
}

//-----------------------------------------------------------------------------------

bool CGame::getPath(){ //BFSD algoritm
	
	/*
		1. Vem prvek z que
		2. Pokud je v set zahoď a zpět na 1.
		3. Sousední políčka dej do que pokud tam není stena
		4. Ulož prvek do map s odpovídající vzdáleností od začátku
		5. Dej prvek do set
		6. Opakuj do doby než je que prázdná
	*/
	/*
		1. Take from queue
		2. If in set back to 1.
		3. Add neighbors to que if not wall
		4. Save to map with distance form begining
		5. Add to set
		6. Do till que is empty
	*/
	
	
	int wave = 0; //Označuje cyklus / cycle number
	int elems = 1; //Pocet prvku v aktuálním cyklu / Number of dest in cycle
	int elemsN = 0; //Pocet prvku v nasledujícím cyklu / Nunber of dest in next cycle

	std::queue<CPoint> que;
	std::set<CPoint> set;
	std::map<CPoint, int> map;
	
	que.push(m_start);
	
	while(!que.empty()){ //6. Opakuj do doby než je que prázdná / Do till que is empty
		CPoint curr = que.front(); // 1. Vem prvek z que / Take from queue
		que.pop();
		elems--;
	
		if(set.find(curr) != set.end()) { //2. Pokud je v set zahoď a zpět na 1. / If in set back to 1.
			if(elems == 0){
			
				wave++;
				elems = elemsN;
				elemsN = 0;	
			}	
			continue; 
		} 
		
		if(m_walls.find(curr.left()) == m_walls.end()){  //3. Sousední políčka dej do que pokud tam není stena / Add neighbors to que if not wall
			que.push(curr.left());
			elemsN++;
		}
		if(m_walls.find(curr.right()) == m_walls.end()){ //3. Sousední políčka dej do que pokud tam není stena / Add neighbors to que if not wall
			que.push(curr.right());
			elemsN++;
		}
		if(m_walls.find(curr.up()) == m_walls.end()){ //3. Sousední políčka dej do que pokud tam není stena / Add neighbors to que if not wall
			que.push(curr.up());
			elemsN++;
		}
		if(m_walls.find(curr.down()) == m_walls.end()){ //3. Sousední políčka dej do que pokud tam není stena / Add neighbors to que if not wall
			que.push(curr.down());
			elemsN++;
		}
		if(m_walls.find(curr.topLeft()) == m_walls.end()){ //3. Sousední políčka dej do que pokud tam není stena / Add neighbors to que if not wall
			que.push(curr.topLeft());
			elemsN++;
		}
		if(m_walls.find(curr.topRight()) == m_walls.end()){ //3. Sousední políčka dej do que pokud tam není stena / Add neighbors to que if not wall
			que.push(curr.topRight());
			elemsN++;
		}
		if(m_walls.find(curr. bottomLeft()) == m_walls.end()){ //3. Sousední políčka dej do que pokud tam není stena / Add neighbors to que if not wall
			que.push(curr. bottomLeft());
			elemsN++;
		}
		if(m_walls.find(curr.bottomRight()) == m_walls.end()){ //3. Sousední políčka dej do que pokud tam není stena / Add neighbors to que if not wall
			que.push(curr.bottomRight());
			elemsN++;
		}
		
		
		map.insert(std::pair<CPoint, int> (curr, wave)); //4. Ulož prvek do map s odpovídající vzdáleností od začátku / Ulož prvek do map s odpovídající vzdáleností od začátku
		
		set.insert(curr); //5. Dej prvek do set / Add to set
		
		if(elems == 0){
			
			wave++;
			elems = elemsN;
			elemsN = 0;	
		}
	
	}	

	if(map.find(m_end) == map.end()) return false;	
	
	m_path.clear();
	m_path.resize(map.find(m_end)->second);
	
	//Rebuid path 
	//Goes from the end to star alway picking the lowest eighbor number
	
	int i = map.find(m_end)->second - 1;
	
	for(auto iter = map.find(m_end); iter->first != m_start; i --){
	
		std::map<CPoint, int>::iterator tmp = iter;
	
		m_path[i] = iter->first;
	
		//check left
		if(map.find(iter->first.left()) != map.end() && tmp->second > map.find(iter->first.left())->second){
		
			tmp = map.find(iter->first.left());
		
		}
		//check right
		if(map.find(iter->first.right()) != map.end() && tmp->second > map.find(iter->first.right())->second){
		
			tmp = map.find(iter->first.right());
		
		} 
		//check up
		if(map.find(iter->first.up()) != map.end() && tmp->second > map.find(iter->first.up())->second){
			
			tmp = map.find(iter->first.up());		
		
		} 
		//check down
		if(map.find(iter->first.down()) != map.end() && tmp->second > map.find(iter->first.down())->second){
		
			tmp = map.find(iter->first.down());		
		
		} 
		//check topLeft
		if(map.find(iter->first.topLeft()) != map.end() && tmp->second > map.find(iter->first.topLeft())->second){
			
			tmp = map.find(iter->first.topLeft());			
		
		} 
		//check topRight
		if(map.find(iter->first.topRight()) != map.end() && tmp->second > map.find(iter->first.topRight())->second){
			
			tmp = map.find(iter->first.topRight());
		
		} 
		//check bottomLeft
		if(map.find(iter->first.bottomLeft()) != map.end() && tmp->second > map.find(iter->first.bottomLeft())->second){
			
			tmp = map.find(iter->first.bottomLeft());
		
		} 
		//check bottomRight
		if(map.find(iter->first.bottomRight()) != map.end() && tmp->second > map.find(iter->first.bottomRight())->second){
			
			tmp = map.find(iter->first.bottomRight());
		
		} 	
		
		iter = tmp;
		
	}
	
	m_path[0] = m_start;
	
	return true;
}

//-----------------------------------------------------------------------------------
/**
 Stucture of save file:
		
			Stats
			"lives"
			"money"
			"kolo"
			"start"
			"end"
			"dim"
			Map
			"x" "y" "type" "info"
			.
			.
			.
			.
			.
			"x" "y" "type" "info"
			"Spawn"
			"Monsters"
			"info"
			.
			.
			.
			"info"
			"Timers"
			"turn" "amount"
			.
			.
			.
			"turn" "amount"
			(
			Path //Optional
			"x" "y" start
			.
			.
			"x" "y" end
			)
			END
 */
bool CGame::save(const std::string & url){

	std::ofstream f( url, std::ios::out | std::ios::app ); //Creates ofstream to file on url 
		
	if( !f ) return false; // Stream not opened
	
	saveStats(f);
	saveMap(f);
	saveSpawn(f);
	savePath(f);
	f << "END";
	
	f.close();
	
	return true;

}

//-----------------------------------------------------------------------------------

void CGame::saveStats(std::ofstream & f){

	f << "Stats" << "\n";
	f << m_lives << "\n";
	f << m_money << "\n";
	f << m_turn << "\n";
	f << m_start.getX() << " " << m_start.getY() << "\n";
	f << m_end.getX() << " " << m_end.getY() << "\n";
	f << m_dim.getX() << " " << m_dim.getY() << "\n";

}

//-----------------------------------------------------------------------------------

void CGame::saveMap(std::ofstream & f){

	std::stringstream buff;

	f << "Map" << "\n";
		for(auto iter = m_map.begin(); iter != m_map.end(); iter++){
		
			f << iter->first.getX() << " " << iter->first.getY() << " ";
			buff.str("");
			iter->second->printf(buff);
			f << buff.str() << "\n";
		
		}
}

//-----------------------------------------------------------------------------------

void CGame::saveSpawn(std::ofstream & f){

		std::stack<CMonster*> tmp;
		std::stringstream buff;

		f << "Spawns" << "\n";
		f << "Monsters" << "\n";
		while(!m_monstersToSpawn.empty()){
		
			CMonster * elem = m_monstersToSpawn.front();
			buff.str("");
			tmp.push(elem);
			elem->printf(buff);
			f << buff.str() << "\n";
			m_monstersToSpawn.pop();
			
		}
		while(!tmp.empty()){
		
			CMonster * elem = tmp.top();
			m_monstersToSpawn.push(elem);
			tmp.pop();
			
		}
			
		f << "Timers" << "\n";
		
		for(auto it = m_spawns.begin(); it != m_spawns.end(); it++){
		
			f << it->first << " " << it->second << "\n";
		
		}	
}

//-----------------------------------------------------------------------------------

void CGame::savePath(std::ofstream & f){

		f << "Path" << "\n";
		
		for(auto it = m_path.begin(); it != m_path.end(); it++){
		
			f << it->getX() << " " << it->getY() << "\n";
		
		}

}

//-----------------------------------------------------------------------------------

bool CGame::load(const std::string & url){
	
	/*
		Struktura save file:
		
			Stats
			"životy"
			"peníze"
			"kolo"
			Map
			"x" "y" "typ" "info"
			.
			.
			.
			.
			.
			"x" "y" "typ" "info"
			"Spawn"
			"Monsters"
			"info"
			.
			.
			.
			"info"
			"Timers"
			"kolo" "pocet"
			.
			.
			.
			"kolo" "pocet"
			(
			Path //volitelně
			"x" "y" //Strat
			.
			.
			"x" "y" //konec
			)
			END
			
			/
			
			Stats
			"lives"
			"money"
			"kolo"
			"start"
			"end"
			"dim"
			Map
			"x" "y" "type" "info"
			.
			.
			.
			.
			.
			"x" "y" "type" "info"
			"Spawn"
			"Monsters"
			"info"
			.
			.
			.
			"info"
			"Timers"
			"turn" "amount"
			.
			.
			.
			"turn" "amount"
			(
			Path //Optional
			"x" "y" //start
			.
			.
			"x" "y" //end
			)
			END
			
	*/
	
	
	std::ifstream f ( url, std::ios::binary | std::ios::in );
	std::string buff;
	
	if( !f ) return false;
	
	getline( f, buff);
	if(buff != "Stats") return false;
	
	getline( f, buff);
	m_lives =  std::stoi(buff);
	
	getline( f, buff);
	m_money =  std::stoi(buff);
	
	getline( f, buff);
	m_turn =  std::stoi(buff);
	
	getline( f, buff);
	m_start = loadPoint(split(buff));
	
	getline( f, buff);
	m_end = loadPoint(split(buff));
	
	getline( f, buff);
	m_dim = loadPoint(split(buff));
	
	getline( f, buff);
	if(buff != "Map") return false;
	
	getline( f, buff);
	while(buff != "Spawns"){ //Načítání mapy / loading map
		
		if(!loadObject(split(buff))) return false;		
		getline( f, buff);
		
	}
	if( !f ) return false;
	
	getline( f, buff);
	if(buff != "Monsters") return false;
	
	getline( f, buff);
	while(buff != "Timers"){ //Načítání MonstersToSpawn / loading monsters to spawn
		
		CMonster * tmp = loadMonster(split(buff), -2);
		if(tmp == nullptr) return false;
		queMonster(tmp);	
		getline( f, buff);
		
	}
	if( !f ) return false;
	
	getline( f, buff);
	
	while(buff != "Path" && buff != "END"){ //Načítání m_spawns / loading m_pawns
		
		if(!loadSpawn(split(buff))) return false;
		getline( f, buff);
		
	}
	
	
	if(buff != "Path"){
	
		getPath();
		getline( f, buff);
		if( f ) return false;
		else return true;
		
	}
	else{
		getline( f, buff);
		while(buff != "END"){ //Načítání m_path / loading path
		
		loadPath(split(buff));
		getline( f, buff);
		}
	
	}
	
	getline( f, buff);
	if( f ) return false;
	
	return true;
}

//-----------------------------------------------------------------------------------

bool CGame::loadObject(const std::vector<std::string> & info){

	if(info.size() < 3) return false; //Nejmenší X Y W (-wall)
	if(info.size() > 8) return false;
	
	int x = std::stoi(info[0]);
	int y = std::stoi(info[1]);
	
	const char * type = info[2].c_str();
	if(strlen(type) > 2) return false;
	
	switch(*type){
		case 'T':{
		
			CTurret * tmp = loadTurret(info);
			if(tmp == nullptr) return false;
			spawnTurret(CPoint(x,y), tmp);
		
			break;
		}
		case 'W':{
		
			CWall * tmp = loadWall(info);
			if(tmp == nullptr) return false;
			spawnWall(CPoint(x,y), tmp);
		
			break;
		}
		case 'M':{
		
			CMonster * tmp = loadMonster(info);
			if(tmp == nullptr) return false;
			spawnMonster(CPoint(x,y), tmp);
		
			break;
		}
		default:{
		
			return false;
		}
	}

	return true;
}

//-----------------------------------------------------------------------------------

CMonster * CGame::loadMonster(const std::vector<std::string> & info, int off){

	//Stuct of info: x y M type atributes...

	const char * type = info[3 + off].c_str();
	
	switch(*type){
		case '%':{
			
			return new CMonster(m_nextId, std::stoi(info[4 + off]), std::stoi(info[5 + off]));
		
			break;
		}
		case '@':{
		
			return new CTank(m_nextId, std::stoi(info[4 + off]), std::stoi(info[5 + off]), std::stoi(info[6 + off]));
		
			break;
		}
		case '&':{
		
			return new CRunner(m_nextId, std::stoi(info[4 + off]), std::stoi(info[5 + off]));
		
			break;
		}
		case '$':{
		
			return new CBoss(m_nextId, std::stoi(info[4 + off]), std::stoi(info[5 + off]));
		
			break;
		}
		default:{
		
			return nullptr;
		}
	}

	return nullptr;
}

//-----------------------------------------------------------------------------------

CTurret * CGame::loadTurret(const std::vector<std::string> & info){

	//Stuct of info: x y T type atributes...

	const char * type = info[3].c_str();
	
	switch(*type){ //Type of turret
		case 'B':{
			
			return new CTurret(m_nextId, std::stoi(info[4]));
		
			break;
		}
		case 'F':{
		
			return new CFrost(m_nextId, std::stoi(info[4]));
		
			break;
		}
		case 'S':{
		
			return new CSniper(m_nextId, std::stoi(info[4]));
		
			break;
		}
		case 'G':{
		
			return new CBFG(m_nextId, std::stoi(info[4]));
		
			break;
		}
		default:{
		
			return nullptr;
		}
	}

	return nullptr;

}

//-----------------------------------------------------------------------------------

CWall * CGame::loadWall(const std::vector<std::string> & info){
	
	//Stuct of info: x y W
	
	return new CWall(m_nextId);
	
}

//-----------------------------------------------------------------------------------

bool CGame::loadSpawn(const std::vector<std::string> & info){

	//Stuct of info: turn amount

	addSpawn(std::stoi(info[0]), std::stoi(info[1]));
	
	return true;
}

//-----------------------------------------------------------------------------------

std::vector<std::string> CGame::split(const std::string & info){

	std::vector<std::string> words;
	std::string word;
	std::istringstream stream(info);
	
	do{
	
		stream >> word;
		words.push_back(word);
	
	} while(stream);
	
	return words;
}

bool CGame::loadPath(const std::vector<std::string> & info){

	m_path.push_back(CPoint(std::stoi(info[0]),std::stoi(info[1])));
	
	return true;
}

//-----------------------------------------------------------------------------------

CPoint CGame::loadPoint(const std::vector<std::string> & info){

	return CPoint(std::stoi(info[0]), std::stoi(info[1]));

}
