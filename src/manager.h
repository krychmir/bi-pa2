#ifndef MANAGER_H
#define MANAGER_H

#include <string>
#include "game.h"
#include <vector>
#include <sstream>
#include <string.h>
/**
 * I/O class of game
 * Translates cmd form user to Game
 * Handles a game status (inProgress, Lost, Won)
 * Output info to user to console
 */
class CManager{

public:

	CManager(); /*!< Does not start a new game */
	~CManager(); /*!< Deletes m_game */

	void input(const std::string & cmd); /*!< Translates a input form user to m_commnads and executes it */
	void print(std::iostream & os) const; /*!< Prints curr state of game and info line */
	

private:

	CGame * m_game; //The game
	std::string m_infoLine; //Has info to user
	
	enum m_commands{ //Used to send control the game
    cNewGame,
    cNextTurn,
    cNotFound,
    cBuildTurret,
    cLvlUpTurret,
    cHelp,
    cTypes,
    cSave
	};
	
	m_commands codeCmd (const std::string & cmd); //Return m_commnads based on cmd
	
	//List of commands 
	void newGame(const std::vector<std::string> & cmd); //Starts a new game
	void nextTurn(const std::vector<std::string> & cmd); //Next turn
	void buildTurret(const std::vector<std::string> & cmd); //Builds a turret
	void lvlUpTurret(const std::vector<std::string> & cmd); //Increses lvl of turret
	void save(const std::vector<std::string> & cmd); //Saves the game
	void help(const std::vector<std::string> & cmd); //Returns a list of commnands
	void types(const std::vector<std::string> & cmd); //Returns info about objets on map
	void notFound(const std::vector<std::string> & cmd); //Default commnad
	
	std::vector<std::string> split(const std::string & cmd); //Splits a string into vector of stings (delim ' ')

};

#endif /* MANAGER_H */
