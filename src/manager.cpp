#include "manager.h"

CManager::CManager(){

	m_game = nullptr;

}

//-----------------------------------------------------------------------------------

CManager::~CManager(){

	if(m_game != nullptr) delete m_game;

}

//-----------------------------------------------------------------------------------

void CManager::input(const std::string & cmd){

	std::vector<std::string> cmdSplit = split(cmd);
	
	switch (codeCmd(*(cmdSplit.begin()))){
	
		case cNewGame: {
		
			newGame(cmdSplit);
		
			break;
		}
		case cNextTurn: {
		
			nextTurn(cmdSplit);
		
			break;
		}
		case cBuildTurret: {
		
			buildTurret(cmdSplit);
		
			break;
		}
		case cLvlUpTurret: {
		
			lvlUpTurret(cmdSplit);
		
			break;
		}
		case cSave: {
		
			save(cmdSplit);
		
			break;
		}
		case cHelp: {
		
			help(cmdSplit);
		
			break;
		}
		case cTypes: {
		
			types(cmdSplit);
		
			break;
		}
		case cNotFound: {
		
			notFound(cmdSplit);
		
			break;
		}
	}
	

}

//-----------------------------------------------------------------------------------

void CManager::print(std::iostream & os) const{

	if(m_game != nullptr){ 
		if(m_game->getLives() < 1){
		
			os << "   _____                       ____                 _ _ \n";
			os << "  / ____|                     / __ \\               | | |\n";
			os << " | |  __  __ _ _ __ ___   ___| |  | |_   _____ _ __| | |\n";
			os << " | | |_ |/ _` | '_ ` _ \\ / _ \\ |  | \\ \\ / / _ \\ '__| | |\n";
			os << " | |__| | (_| | | | | | |  __/ |__| |\\ V /  __/ |  |_|_|\n";
			os << "  \\_____|\\__,_|_| |_| |_|\\___|\\____/  \\_/ \\___|_|  (_|_)\n";
			os << "\n";                                                  
		}
		else if(m_game->getWin()){
		
			os << " __     __                    _       _ _     _____  \n";
			os << " \\ \\   / /                   (_)     | | |  _|  __ \\ \n";
			os << "  \\ \\_/ /__  _   _  __      ___ _ __ | | | (_) |  | |\n";
			os << "   \\   / _ \\| | | | \\ \\ /\\ / / | '_ \\| | |   | |  | |\n";
			os << "    | | (_) | |_| |  \\ V  V /| | | | |_|_|  _| |__| |\n";
			os << "    |_|\\___/ \\__,_|   \\_/\\_/ |_|_| |_(_|_) (_)_____/ \n";
			os << "\n";                                                  		
		
		}
		else m_game->print(os);
	}
	os << m_infoLine;
	
}

//-----------------------------------------------------------------------------------

CManager::m_commands CManager::codeCmd(const std::string & cmd){

	if (cmd == "NewGame" || cmd == "G") return cNewGame;
  if (cmd == "NextTurn" || cmd == "T") return cNextTurn;
  if (cmd == "BuildTurret" || cmd == "B") return cBuildTurret;
  if (cmd == "LevelUpTurret" || cmd == "L") return cLvlUpTurret;
  if (cmd == "Help" || cmd == "?") return cHelp;
  if (cmd == "Save" || cmd == "S") return cSave;
  if (cmd == "Types" || cmd == "T?") return cTypes;
	return cNotFound;
	
}

//--------------------------CMDS-----------------------------------------------------

void CManager::newGame(const std::vector<std::string> & cmd){

	if(cmd.size() == 2){
	
		if(m_game != nullptr){ 
			delete m_game;
			m_game = new CGame();
		}
		else{
			m_game = new CGame();
		}
		m_infoLine = "New game started.\n";
		
	}
	else if(cmd.size() == 3){
	
		if(m_game != nullptr){ 
			delete m_game;
			
			try{
				m_game = new CGame(cmd[2]);
				m_infoLine = "New game started.\n";
			}
			catch(std::string error){
				m_infoLine = error + ".\n";
			}
		}
		else{
			try{
				m_game = new CGame(cmd[2]);
				m_infoLine = "New game started.\n";
			}
			catch(std::string error){
				m_infoLine = error + ".\n";
			}
		}
		
	
	}
	else{
		m_infoLine = "Use NewGame or G (-path to save)\n";
	}

}

//-----------------------------------------------------------------------------------

void CManager::nextTurn(const std::vector<std::string> & cmd){

	if(cmd.size() != 2){
		m_infoLine = "Use NextTurn or T.\n";
		return;
	}

	if(m_game != nullptr){ 
	
		m_game->nextTurn();
		m_infoLine = "Next turn.\n";
	
	}
	else m_infoLine = "No game in progress.\n";
	
}

//-----------------------------------------------------------------------------------

void CManager::buildTurret(const std::vector<std::string> & cmd){

	if(m_game == nullptr){ 
	
		m_infoLine = "No game in progress.\n";
		return;
		
	}

	if(cmd.size() == 5){
	
		const char * type = cmd[1].c_str();
		if(strlen(type) > 2){
			m_infoLine = "Invalid turret type.\n";
			return;
		}
	
		if(m_game->buidTurret(type[0], std::stoi(cmd[2]), std::stoi(cmd[3]))){
			m_infoLine = "Turret buid.\n";
			return;
		}
		else{
			m_infoLine = "Not enough money/invalid X|Y.\n";
			return;
		}
	
	}
	else m_infoLine = "Use BuidTurret or B (-type, x, y).\n";

}

//-----------------------------------------------------------------------------------

void CManager::lvlUpTurret(const std::vector<std::string> & cmd){

	if(m_game == nullptr){ 
	
		m_infoLine = "No game in progress.\n";
		return;
		
	}
	if(cmd.size() == 4){
	
	
		if(m_game->lvlUpTurret(std::stoi(cmd[1]), std::stoi(cmd[2]))){
			m_infoLine = "Turret lvl++.\n";
			return;
		}
		else{
			m_infoLine = "Not enough money/invalid X|Y.\n";
			return;
		}
	
	}
	else m_infoLine = "Use LevelUpTurret or L (-x, y).\n";

}

//-----------------------------------------------------------------------------------

void CManager::save(const std::vector<std::string> & cmd){

	if(m_game == nullptr){ 
	
		m_infoLine = "No game in progress.\n";
		return;
		
	}
	if(cmd.size() == 3){
	
		m_game->save(cmd[1]);
		m_infoLine = "Game saved.\n";

	}
	else m_infoLine = "Use Save or s (-url).\n";
}

//-----------------------------------------------------------------------------------

void CManager::help(const std::vector<std::string> & cmd){

	m_infoLine = 	"List of commands\n";
	m_infoLine += "G/NewGame (-url) for a new game.\n";
	m_infoLine += "B/BuidTurret (-type, x, y) to build.\n";
	m_infoLine += "L/LevelUpTurret (-x, y) to levelUp a turret.\n";
	m_infoLine += "T/NextTurn for next turn.\n";
	m_infoLine += "S/Save (-url) to save game.\n";
	m_infoLine += "?/Help to se this.\n";

}

//-----------------------------------------------------------------------------------

void CManager::types(const std::vector<std::string> & cmd){

	m_infoLine = 	"Turrets\n";
	m_infoLine += "B - basic turret\n";
	m_infoLine += "F - freeze turret\n";
	m_infoLine += "S - sniper turret\n";
	m_infoLine += "G - The BFG\n";
	m_infoLine += "Monsters\n";
	m_infoLine += "% - basic monster";
	m_infoLine += "@ - tank\n";
	m_infoLine += "& - runner\n";
	m_infoLine += "$ - boss\n";
	m_infoLine += "Wall -> #\n";

}

//-----------------------------------------------------------------------------------

void CManager::notFound(const std::vector<std::string> & cmd){

	m_infoLine = "Unkown commnad.\n";

}

//-----------------------------------------------------------------------------------

std::vector<std::string> CManager::split(const std::string & cmd){

	std::vector<std::string> words;
	std::string word;
	std::istringstream stream(cmd);
	
	do{
	
		stream >> word;
		words.push_back(word);
	
	} while(stream);
	
	return words;

}

//-----------------------------------------------------------------------------------
