#ifndef GAME_H
#define GAME_H

#include <vector>
#include <iostream>
#include <string>
#include <map>
#include <set>
#include <queue>
#include <stack>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <functional>
#include <iostream>
#include <string.h>

#include "monster.h"
#include "object.h"
#include "turret.h"
#include "wall.h"
#include "point.h"
/**
 * Main class of the game.
 * Game cycle: 
 * 1. Move all moster
 * 2. If monster ready do spawn, spawn it
 * 3. Fire all turrets
 * 4. Remove mosterst with hp < 0 and add money
 * 5. Check the exit. If monster, kill it and remove live
 */
class CGame{

public:

	CGame(); /*!< Starts a gefault game */
	CGame(const std::string & url); /*!< Starts a game form a file form url */
	~CGame(); /*!< Deletes all CObjects from m_map and all CMonsters from m_monsters to spawn */
	
	void nextTurn(); /*!< Makes the game cycle happen. */

	bool buidTurret(char type, int x, int y); /*!< Type specifies what turret will be buid and x, y where. */
	bool lvlUpTurret(int x, int y); /*!< Increses lvl of a turrnet on x/y. Used by CManager */
	
	void print(std::iostream & os) const; /*!< Returns current state of game. */
	int getLives(); /*!< Returns current live points. */
	bool getWin(); /*!< Returns false if game is in progress and true if game is won */
	
	bool save(const std::string & url); /*!< Saves the game to a file specified by url */

private:

	std::multimap<CPoint, CObject*> m_map; //Multimap of all CObjects
	
	std::map<CPoint, CWall*> m_walls; //Map of all walls
	std::multimap<CPoint, CMonster*> m_monsters; //Multimap of all monsters
	std::map<CPoint, CTurret*> m_turrets; //Map of all turrets
	std::vector<CPoint> m_path; //Path from start to exit
	
	std::map<int, int> m_spawns; //Pair first: turn, second: how much
	std::queue<CMonster*> m_monstersToSpawn; //Queued monsters

	int m_lives; //Lives left
	int m_money; //Money
	int m_nextId; //Id of next object
	int m_turn; //Current turn
	bool m_win; //False: Game in progres // True: game is won
	
	CPoint m_start; // Point where monsters appear
	CPoint m_end; // Home base. Target of monsters
	CPoint m_dim; // The monst x and y point on the map
	
	bool getPath(); //Uses BFSD to find the shortest path to end
	void moveMonsters(); //Moves all mosnters
	void moveMonster(const CPoint & point, CMonster * mons); //Move monster to point
	bool deleteMonster(std::multimap<CPoint, CMonster*>::iterator & mons, bool fullDell = false); //Deletes monster from map. If fullDell is true it will use delete
	void fireTurrets(); // Fires all turrets. Fires on o monsters closest to end
	void endTurn(); //Ends the turn
	void spawnMonsters(); //Spawns monster from queue
	
	void spawnTurret(const CPoint & point, CTurret * turr); //Adds the turret to m_turrets and m_map
	void spawnMonster(const CPoint & point, CMonster * mons); //Adds the monstet to m_mosters and m_map
	void spawnWall(const CPoint & point, CWall * wall); //Adds wall to m_walls and m_map
	void queMonster(CMonster * mons); //Adds monster to m_monstersToSpawn
	void addSpawn(int turn, int amount); //Adds info about spawning monster to m_spawns
	
	void saveStats(std::ofstream & f); //Saves game stats (lives, money...)
	void saveMap(std::ofstream & f); //Saves m_map
	void saveSpawn(std::ofstream & f); //Saves m_monstersToSpawn and m_spawns
	void savePath(std::ofstream & f); //Saves m_path
	
	bool load(const std::string & url); /*!< Loads the game from a file specified by url */
	
	bool loadObject(const std::vector<std::string> & info); //Loads the a CObject specified by info
	CMonster * loadMonster(const std::vector<std::string> & info, int off = 0); //Creates a CMonster specified by info. Off to mark where info starts (default + 2)
	CTurret * loadTurret(const std::vector<std::string> & info); //Creates a CTurret specifiend by info;
	CWall * loadWall(const std::vector<std::string> & info); //Creates a wall;
	bool loadSpawn(const std::vector<std::string> & info); //Loads info about spawnning monsters in to m_spawns
	bool loadPath(const std::vector<std::string> & info); //Loads path to m_path
	CPoint loadPoint(const std::vector<std::string> & info); //Loads a point
	
	std::vector<std::string> split(const std::string & info); //Splits a string into vector of stings (delim ' ')
	
};

#endif /* GAME_H */
