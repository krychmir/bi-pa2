#include "object.h"

#ifndef MONSTER_H
#define MONSTER_H
/**
 * Class used to represent monster attacking player base
 * Child class of CObject
 * Parent class to other special monsters
 */
class CMonster : public CObject {

public:

	CMonster(int id, int hp, int att): /*!< Constructor. Sets all params for monster */
		CObject(id),
		m_hp(hp),
		m_att(att),
		m_freeze(0),
		m_alive(true),
		m_moved(0)
	{}; 
	
	virtual bool getHit(int attDmg); /*!< Virtual class. Returns true if monster can get hit else false */
	virtual int moves(); /*!< Virtual class that return number of tiles monster can move in a turn */
	virtual void getFrozen(int turns); /*!< Virtual class that frezes a monster */
	virtual void turn(); /*!< Virtual class that sets parmans for next turn */
	virtual int getMoved(); /*!< Virtual class that teturns number of moves done by monster */
	virtual void setMoved(int val); /*!< Virtual class that sets moved */
	virtual int getAttack(); /*!< Virtual class that return monsters att */
	virtual bool isAlive(); /*!< Virtual class. Returns true if alive and false if dead */
	virtual int moneyInPocket(); /*!< Virtual class. Returns bounty for killing a monster */
	
	virtual void print(std::iostream & os) const; /*!< Virtual print for output to user */
	virtual void printf(std::iostream & f) const; /*!< Virtual print for saving a game */
	
protected:

	int m_hp; /*!< Hp left */
	int m_att; /*!< Monsters attack */
	int m_freeze; /*!< if 0 monster not frozen else means number of turn that a mnster will be frozen */
	bool m_alive; /*!< True if monster is alive else false */
	int m_moved; /*!< Number of turn that the monster moved */

};

//-----------------------------------------------------------------------------------
/**
 * Class used to represent a tank monster attacking player base
 * Child class of CMonster
 * Is more durable than normal monster (has armor)
 * Armor reduces incoming dmg from turret by a flat amount
 */
class CTank : public CMonster {

public:

	CTank(int id, int hp, int att, int armor): /*!< Constructor. Has extra param armor */
		CMonster(id, hp, att),
		m_armor(armor)
	{}; 
	
	virtual bool getHit(int attDmg); /*!< Overloading virtual getHit from monster. Armor reduces dmg taken */
	
	virtual void print(std::iostream & os) const; /*!< Virtual print for output to user */
	virtual void printf(std::iostream & f) const; /*!< Virtual print for saving a game */

protected:

	int m_armor;  /*!< Flat amount of armor. Reduces inc damage */

};

//-----------------------------------------------------------------------------------
/**
 * Class used to represent a runner monster attacking player base
 * Child class of CMonster
 * Is faster than normal monster moves twice as fast
 */
class CRunner : public CMonster {

public:

	CRunner(int id, int hp, int att): /*!< Constructor. No diff from CMonster */
		CMonster(id, hp, att)
	{}; 

	virtual int moves(); /*!< Overloading virtual moves so the runner can go faster */
	
	virtual void print(std::iostream & os) const; /*!< Virtual print for output to user */
	virtual void printf(std::iostream & f) const; /*!< Virtual print for saving a game */

protected:



};

//-----------------------------------------------------------------------------------
/**
 * Class used to represent a boss monster attacking player base
 * Child class of CMonster
 * Has twice the hp a attack than a normal monster
 */
class CBoss : public CMonster {

public:

	CBoss(int id, int hp, int att): /*!< Constructor. Makes a CMOnster with double hp and attack */
		CMonster(id, hp * 2, att * 2)
	{}; 
	
	virtual void print(std::iostream & os) const; /*!< Virtual print for output to user */
	virtual void printf(std::iostream & f) const; /*!< Virtual print for saving a game */

protected:



};


#endif /* MONSTER_H */
