#include "monster.h"

//xxxxxxxxxxxxxxxxxxxxxxxxMONSTERxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

bool CMonster::getHit(int dmg){

	if(m_alive){		
		m_hp -= dmg;
		if(m_hp < 1) m_alive = false;
	}
	
	else return false;
	return true;
	
}

//-----------------------------------------------------------------------------------

int CMonster::moves(){

	int moves = 1;
	if(m_freeze) moves /= 2;
	
	if(moves < 1)	return 1;
	
	else return moves;

}

//-----------------------------------------------------------------------------------

void CMonster::print(std::iostream & os) const{

 os << "%";

}

//-----------------------------------------------------------------------------------

void CMonster::getFrozen(int turns){

	if(turns > m_freeze) m_freeze = turns;

}

//-----------------------------------------------------------------------------------

void CMonster::turn(){

	if(m_freeze > 0) m_freeze--;
	m_moved = 0;

}

//-----------------------------------------------------------------------------------

void CMonster::setMoved(int val){

	m_moved = val;

}

//-----------------------------------------------------------------------------------

int CMonster::getMoved(){

	return m_moved;

}

//-----------------------------------------------------------------------------------

int CMonster::getAttack(){

	return m_att;

}

//-----------------------------------------------------------------------------------

bool CMonster::isAlive(){

	return m_alive;

}

//-----------------------------------------------------------------------------------

int CMonster::moneyInPocket(){

	return m_att + m_hp/10;

}

//-----------------------------------------------------------------------------------

void CMonster::printf(std::iostream & f) const{

	f  << "M" << " " << "%" << " " << m_hp << " " << m_att;

}

//xxxxxxxxxxxxxxxxxxxxxxxxTANKxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

bool CTank::getHit(int attDmg){

	if(m_alive){		
		if(attDmg > m_armor) m_hp -= attDmg - m_armor;
		if(m_hp < 1) m_alive = false;
	}
	
	else return false;
	return true;

}

//-----------------------------------------------------------------------------------

void CTank::print(std::iostream & os) const{

 os << "@";

}

//-----------------------------------------------------------------------------------

void CTank::printf(std::iostream & f) const{

	f << "M" << " " << "@" << " " << m_hp << " " << m_att << " " << m_armor;

}

//xxxxxxxxxxxxxxxxxxxxxxxxRUNNERxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

int CRunner::moves(){

	int moves = 2;
	if(m_freeze) moves /= 2;
	
	if(moves < 1)	return 1;
	
	else return moves;

}

//-----------------------------------------------------------------------------------

void CRunner::print(std::iostream & os) const{

 os << "&";

}

//-----------------------------------------------------------------------------------

void CRunner::printf(std::iostream & f) const{

	f << "M" << " " << "&" << " " << m_hp << " " << m_att;

}

//xxxxxxxxxxxxxxxxxxxxxxxxBOSSxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

void CBoss::print(std::iostream & os) const{

 os << "$";

}

//-----------------------------------------------------------------------------------

void CBoss::printf(std::iostream & f) const{

	f << "M" << " " << "$" << " " << m_hp/2 << " " << m_att/2;

}

