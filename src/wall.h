#ifndef WALL_H
#define WALL_H

#include <iostream>
#include "object.h"
/**
 * Class used to represent a inpassable object for monsters
 * Child class of CObject
 * Important for searching for path
 */
class CWall : public CObject{

public:
	
	CWall(int id): /*!< Constructor. */
		CObject(id)
	{};

	virtual void print(std::iostream & os) const; /*!< Virtual print for output to user */
	virtual void printf(std::iostream & f) const; /*!< Virtual print for saving a game */

protected:

	

};

#endif /* WALL_H */
