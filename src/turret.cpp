#include "turret.h"

//xxxxxxxxxxxxxxxxxxxxxxxxTURRETxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

int CTurret::range(){

	return 2;

}

//-----------------------------------------------------------------------------------

int CTurret::attDmg(){

	return (20 + (20 * 0.5 * m_lvl));

}

//-----------------------------------------------------------------------------------

void CTurret::attack(CMonster & monster){

	monster.getHit(attDmg());

}

//----------------------------------------------------------------------------------- 

void CTurret::print(std::iostream & os) const{

	os << "B";

}

//----------------------------------------------------------------------------------- 

void CTurret::lvlUp(){

	m_lvl++;

}

//----------------------------------------------------------------------------------- 

int CTurret::costLvlUp(){

	return m_lvl * 50;

}

//----------------------------------------------------------------------------------- 

int CTurret::costToBuild(){

	return 100;

}

//----------------------------------------------------------------------------------- 

int CTurret::getLvl(){

	return m_lvl;

}

//----------------------------------------------------------------------------------- 

void CTurret::printf(std::iostream & f) const{

	f << "T" << " " << "B" << " " << m_lvl;

}

//xxxxxxxxxxxxxxxxxxxxxxxxSNIPERxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

int CSniper::range(){

	return 6;

}

//----------------------------------------------------------------------------------- 

void CSniper::print(std::iostream & os) const{

	os << "S";

}

//----------------------------------------------------------------------------------- 

void CSniper::printf(std::iostream & f) const{

	f << "T" << " " << "S" << " " << m_lvl;

}

//----------------------------------------------------------------------------------- 

int CSniper::costLvlUp(){

	return m_lvl * 80;

}

//----------------------------------------------------------------------------------- 

int CSniper::costToBuild(){

	return 200;

}

//xxxxxxxxxxxxxxxxxxxxxxxxFROSTxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

void CFrost::attack(CMonster & monster){

	monster.getHit(attDmg());
	monster.getFrozen(2);

}

//----------------------------------------------------------------------------------- 

void CFrost::print(std::iostream & os) const{

	os << "F";

}

//----------------------------------------------------------------------------------- 

void CFrost::printf(std::iostream & f) const{

	f << "T" << " " << "F" << " " << m_lvl;

}

//----------------------------------------------------------------------------------- 

int CFrost::costLvlUp(){

	return m_lvl * 60;

}

//----------------------------------------------------------------------------------- 

int CFrost::costToBuild(){

	return 150;

}

//xxxxxxxxxxxxxxxxxxxxxxxxBFGxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

int CBFG::attDmg(){

	return (50 + (50 * m_lvl));

}

//----------------------------------------------------------------------------------- 

void CBFG::print(std::iostream & os) const{

	os << "G";

}

//----------------------------------------------------------------------------------- 

void CBFG::printf(std::iostream & f) const{

	f << "T" << " " << "G" << " " << m_lvl;

}

//----------------------------------------------------------------------------------- 

int CBFG::costLvlUp(){

	return m_lvl * 200;

}

//----------------------------------------------------------------------------------- 

int CBFG::costToBuild(){

	return m_lvl * 500;

}



