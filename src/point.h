#ifndef POINT_H
#define POINT_H

#include <stdlib.h>
#include <iostream>
/**
 * Class used as a key to std::map and std::multimap in CGame
 * Also used to represent path from star to end
 * Made so monsters can move diagonally, horizontally and vertically
 * Has all requirement to be a key in map 
 */
class CPoint{

public:

	CPoint();  /*!< Default Constructor, sets m_y and m_x to 0 */
	CPoint(int x, int y); /*!< Constructor, sets m_y to y and m_x to x */
	
	bool operator < ( const CPoint & point ) const;  /*!< Overloaded op for comparing CPoints */
	bool operator == ( const CPoint & point ) const; /*!< Overloaded op for comparing CPoints */
	bool operator != ( const CPoint & point ) const; /*!< Overloaded op for comparing CPoints */
	int operator - ( const CPoint & point ) const; /*!< Overloaded op for comparing CPoints */
	
	CPoint left() const; /*!< Returns left neighbor CPoint */
	CPoint right() const; /*!< Returns right neighbor CPoint */
	CPoint up() const; /*!< Returns up neighbor CPoint */
	CPoint down() const; /*!< Returns down neighbor CPoint */
	CPoint topLeft() const; /*!< Returns top left neighbor CPoint */
	CPoint topRight() const; /*!< Returns top rigt neighbor CPoint */
	CPoint bottomLeft() const; /*!< Returns bottom left neighbor CPoint */
	CPoint bottomRight() const; /*!< Returns bottom right neighbor CPoint */
	 
	int getX()const /*!< Returns m_x */ { return m_x; }; 
	int getY()const /*!< Returns m_y */ { return m_y; }; 
	
private:

 int m_x; //X cordinate
 int m_y; //Y cordinate
 
 int Vall() const; //Return vall of point

};

#endif /* POINT_H */
