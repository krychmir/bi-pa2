#include "game.h"
#include "manager.h"
#include <iostream>
#include <sstream> 
#include <string> 

using namespace std;

int main(void){
	
	CManager man = CManager();
	string in;
	stringstream out;
	
	cout << "DawnDefense V0.1. Help for list of commnands. Enjoy" << endl;
	
	while(true){
	
		getline( cin, in);
		
		if(in == "Exit") break;
		
		man.input(in);
		man.print(out);
	
		cout << out.str();
		out.str("");
	
	}

	return 0;
}
