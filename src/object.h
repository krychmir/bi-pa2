#ifndef OBJECT_H
#define OBJECT_H

#include <iostream>
/**
 * Parrent class to objects in CGame
 * Used for output, searching and saving purpuses
 */
class CObject{

public:

	CObject(int id): /*!< Constructor, sets id */
		m_id(id)
	{};
	virtual ~CObject() = default; /*!< Virtual default destructor */
	
	int getId() { return m_id; } /*!< Returns id */
	
	virtual void print(std::iostream & os) const; /*!< Virtual print for output to user */
	virtual void printf(std::iostream & f) const; /*!< Virtual print for saving a game */
	
	friend std::iostream & operator << (std::iostream & os, const CObject & obj){

		obj.print(os);
		return os;

	} /*!< Overloded operator <<. Uses virtual print() */
	
protected:

	int m_id; /*!< ID of object */

};

#endif /* OBJECT_H */
