#include "point.h"


CPoint::CPoint():
	m_x(0),
	m_y(0)	
{}

//-----------------------------------------------------------------------------------

CPoint::CPoint(int x, int y){

	if(x > 0 && y > 0){
		m_x = x;
		m_y = y;	
	}
	else{
		m_x = -1;
		m_y = -1;
	}
}

//-----------------------------------------------------------------------------------

int CPoint::Vall() const{

	return m_x + ( (m_y-1) * m_x );

}

//-----------------------------------------------------------------------------------
/**
 * Returns absolute distance betwen two points
 */
int CPoint::operator - ( const CPoint & point ) const{

	if(abs(this->m_y - point.m_y) >= abs(this->m_x - point.m_x)) return abs(this->m_y - point.m_y);
	else return abs(this->m_x - point.m_x);

}

//-----------------------------------------------------------------------------------
/**
 * Compares two point based on x and y
 * Ex:
 * CPoint(0,0) < CPoint(1,0) < CPoint(0,1) < CPoint(1,1)
 */
bool CPoint::operator < ( const CPoint & point ) const{

	if (this->m_y < point.m_y) return true;
	else if (this->m_y > point.m_y) return false;
	else if (this->m_x < point.m_x) return true;
	else return false;

}

//-----------------------------------------------------------------------------------
/**
 * Returns true if CPoints has the same m_x and m_y else false
 */
bool CPoint::operator == ( const CPoint & point ) const{

	if(this->m_y == point.m_y && this->m_x == point.m_x) return true;
	else return false;
	
}

//-----------------------------------------------------------------------------------
/**
 * Returns false if CPoints has the same m_x and m_y else true
 */
bool CPoint::operator != ( const CPoint & point ) const{

	if(this->m_y != point.m_y || this->m_x != point.m_x) return true;
	else return false;
	
}

//-----------------------------------------------------------------------------------

CPoint CPoint::left() const{

	return CPoint(m_x - 1, m_y);

}

//-----------------------------------------------------------------------------------

CPoint CPoint::right() const{

	return CPoint(m_x + 1, m_y);

}

//-----------------------------------------------------------------------------------

CPoint CPoint::up() const{

	return CPoint(m_x, m_y + 1);

}

//-----------------------------------------------------------------------------------

CPoint CPoint::down() const{

	return CPoint(m_x, m_y - 1);

}

//-----------------------------------------------------------------------------------

CPoint CPoint::topLeft() const{

	return CPoint(m_x - 1, m_y + 1);

}

//-----------------------------------------------------------------------------------

CPoint CPoint::topRight() const{

	return CPoint(m_x + 1, m_y + 1);

}

//-----------------------------------------------------------------------------------

CPoint CPoint::bottomLeft() const{

	return CPoint(m_x - 1, m_y - 1);

}

//-----------------------------------------------------------------------------------

CPoint CPoint::bottomRight() const{

	return CPoint(m_x + 1, m_y - 1);

}

//-----------------------------------------------------------------------------------

