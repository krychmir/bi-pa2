#include "object.h"
#include "monster.h"

#ifndef TURRET_H
#define TURRET_H
/**
 * Class used to represent a basic turret
 * Child class of CObject
 * Can shoot a monster. Has low atributes and cost
 */
class CTurret : public CObject {

public:

	CTurret(int id, int lvl): /*!< Constructor sets a m_lvl */
		CObject(id),
		m_lvl(lvl)
	{}; 
	
	virtual void lvlUp(); /*!< Adds m_lvl by one */
	virtual int costLvlUp(); /*!< Returns cost to lvlUp a turret */
	virtual int costToBuild(); /*!< Returns cost to build a lvl one turret */
	virtual int range(); /*!< Returns number of point turret can shoot */
	virtual int attDmg(); /*!< Returns attDmg of turret */
	virtual void attack(CMonster & monster); /*!< Turret shots a monster */
	virtual int getLvl(); /*!< Returns lvlOf turret */
	
	virtual void print(std::iostream & os) const; /*!< Virtual print for output to user */
	virtual void printf(std::iostream & f) const; /*!< Virtual print for saving a game */
	
	
protected:

	int m_lvl; /*!< Turrets lvl / higher lvl -> more damage/more cost */

};

//-----------------------------------------------------------------------------------
/**
 * Class used to represent a sniper turret
 * Child class of CTurret
 * Has high range and incresed cost
 */
class CSniper : public CTurret {

public:

	CSniper(int id, int lvl): /*!< Constructor sets a m_lvl. Same as CTurret*/
		CTurret(id, lvl)
	{};
	
	virtual int range(); /*!< Overloaded. Has more range than CTurret */
	virtual int costLvlUp(); /*!< Overloaded it is more costly to lvlUp */
	virtual int costToBuild(); /*!< Overloaded it is more costly to build */
	
	virtual void print(std::iostream & os) const; /*!< Virtual print for output to user */
	virtual void printf(std::iostream & f) const; /*!< Virtual print for saving a game */
	
protected:



};

//-----------------------------------------------------------------------------------
/**
 * Class used to represent a frost turret
 * Child class of CTurret
 * When it hits a moster it freezes it for x-number of turns. Moderately expensive
 */
class CFrost : public CTurret {

public:

	CFrost(int id, int lvl): /*!< Constructor sets a m_lvl. Same as CTurret*/
		CTurret(id, lvl)
	{};
	
	virtual void attack(CMonster & monster); /*!< Overloaded freazes a monster */
	virtual int costLvlUp(); /*!< Overloaded it is more costly to lvlUp */
	virtual int costToBuild(); /*!< Overloaded it is more costly to build */
	
	virtual void print(std::iostream & os) const; /*!< Virtual print for output to user */
	virtual void printf(std::iostream & f) const; /*!< Virtual print for saving a game */

};

//-----------------------------------------------------------------------------------
/**
 * Class used to represent a the BFG turret
 * Child class of CTurret
 * Has much more attack than normal turret but it is very expensive
 */
class CBFG : public CTurret {

public:

	CBFG(int id, int lvl): /*!< Constructor sets a m_lvl. Same as CTurret*/
		CTurret(id, lvl)
	{};
	
	virtual int attDmg(); /*!< Overloaded deals more damage */
	virtual int costLvlUp(); /*!< Overloaded it is more costly to lvlUp */
	virtual int costToBuild(); /*!< Overloaded it is more costly to build */
	
	virtual void print(std::iostream & os) const; /*!< Virtual print for output to user */
	virtual void printf(std::iostream & f) const; /*!< Virtual print for saving a game */

};

#endif /* TURRET_H */
